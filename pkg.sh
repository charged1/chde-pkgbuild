#!/usr/bin/env bash

# Stop script if an error occurs anywhere.
set -e

# Welcome
echo "###########################################"
echo "## Welcome! What are we packaging today? ##"
echo "###########################################"
# Asking user for the package.
read packagename

# Enter the package directory.
cd x86_64/$packagename

# Setting variables.
nameofpkg=$(awk -F "=" '/^pkgname=/ {print $NF}' PKGBUILD)
underlinenameofpkg=$(awk -F "=" '/^_pkgname=/ {print $NF}' PKGBUILD)
oldversionofpkg=$(awk -F "=" '/^pkgver=/ {print $NF}' PKGBUILD)
repodir=("/home/charged/gl/ariz/ariz-core-repo/x86_64/")
# repodir=("/home/charged/gl/ariz/ariz-3party-repo/x86_64/")

# Removing old package.
rm $repodir$nameofpkg-$oldversionofpkg-1-any.pkg.tar.zst | true

# Building
echo "#########################"
echo "## Building package... ##"
echo "#########################"

# MAKEPKG
makepkg -sicf

# Cleaning
echo "###################################"
echo "## Transferring package to repo. ##"
echo "###################################"

# Removing excess packages.
rm -rf ariz-configs/ pkg src
rm -rf $underlinenameofpkg

# Reset variables.
newversionofpkg=$(awk -F "=" '/^pkgver=/ {print $NF}' PKGBUILD)

# New var.
archpkg_unedited=$(awk -F "=" '/^arch/ {print $NF}' PKGBUILD)

if [ "$archpkg_unedited" == "('any')" ]; then
    archpkg="any"
elif [ "$archpkg_unedited" == "('x86_64')" ]; then
    archpkg="x86_64"
fi

# Move package to repo.
mv $nameofpkg-$newversionofpkg-1-$archpkg.pkg.tar.zst $repodir

# Push to GitLab.
echo "########################"
echo "## Pushing to GitLab. ##"
echo "########################"

git add .
git commit -m "Update PKGBUILDs."
git push

# Run script in repo.
echo "########################"
echo "## Updating database. ##"
echo "########################"

cd $repodir
cd ../
./build.sh

echo "########################"
echo "## Pushing to GitLab. ##"
echo "########################"

# Push repo.
git add .
git commit -m "Update repo."
git push

echo "#########################"
echo "## All tasks complete! ##"
echo "#########################"
